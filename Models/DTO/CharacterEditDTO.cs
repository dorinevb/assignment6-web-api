﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.DTO
{
    public class CharacterEditDTO
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required][MaxLength(50)] public string FullName { get; set; } = string.Empty;
        [MaxLength(50)] public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string PictureUrl { get; set; } = string.Empty;
    }
}
