﻿using MovieCharacterAPI.Models.Domain;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.DTO
{
    public class FranchiseReadDTO
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required][MaxLength(50)] public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
