﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.DTO
{
    public class FranchiseCreateDTO
    {
        // Fields
        [Required][MaxLength(50)] public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
