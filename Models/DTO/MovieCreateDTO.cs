﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.DTO
{
    public class MovieCreateDTO
    {
        // Fields
        [Required][MaxLength(50)] public string Title { get; set; } = string.Empty;
        [MaxLength(50)] public string Genre { get; set; } = string.Empty;
        public int ReleaseYear { get; set; }
        [MaxLength(50)] public string? Director { get; set; }
        public string PictureUrl { get; set; } = string.Empty;
        public string TrailerUrl { get; set; } = string.Empty;
        // Foreign key
        public int FranchiseId { get; set; }
    }
}
