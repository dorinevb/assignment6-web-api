﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{
    public class Franchise
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required] [MaxLength(50)] public string Name { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        // Navigation collection property
        public ICollection<Movie?> Movies { get; set; } = new Collection<Movie?>();
    }
}
