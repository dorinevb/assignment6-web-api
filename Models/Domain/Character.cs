﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{

    public class Character
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required] [MaxLength(50)] public string FullName { get; set; } = string.Empty;
        [MaxLength(50)] public string? Alias { get; set; }
        public string? Gender { get; set; }
        public string PictureUrl { get; set; } = string.Empty;

        // Navigation collection property
        public ICollection<Movie> Movies { get; set; } = new Collection<Movie>();
    }
}
