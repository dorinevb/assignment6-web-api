﻿using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;

namespace MovieCharacterAPI.Models.Domain
{
    public class Movie
    {
        // Primary key
        public int Id { get; set; }
        // Fields
        [Required] [MaxLength(50)] public string Title { get; set; } = string.Empty;
        [MaxLength(50)] public string Genre { get; set; } = string.Empty;
        public int ReleaseYear { get; set; }
        [MaxLength(50)] public string? Director { get; set; }
        public string PictureUrl { get; set; } = string.Empty;
        public string TrailerUrl { get; set; } = string.Empty;
        // Foreign key
        public int FranchiseId { get; set; }
        // Navigation property
        public Franchise? Franchise { get; set; }
        // Navigation collection property
        public ICollection<Character?> Characters { get; set; } = new Collection<Character?>();
    }
}
