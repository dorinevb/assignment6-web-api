﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Models
{
    public class MovieCharacterDbContext : DbContext
    {
        public MovieCharacterDbContext(DbContextOptions<MovieCharacterDbContext> options) : base(options)
        {
        }

        public DbSet<Franchise> Franchises => Set<Franchise>();
        public DbSet<Movie> Movies => Set<Movie>();
        public DbSet<Character> Characters => Set<Character>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Seed data
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Wizarding World", Description = "The Wizarding World is a fantasy media franchise including 8 Harry Potter movies and 3 Fantastic Beasts movies." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "Batman", Description = "The Batman Franchise has produced many Batman movies since 1943." });

            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 1, Title = "Harry Potter and the Philosopher's stone", Genre = "fantasy", ReleaseYear = 2001, Director = "Chris Columbus", PictureUrl = "https://en.wikipedia.org/wiki/Harry_Potter_and_the_Philosopher%27s_Stone_(film)#/media/File:Harry_Potter_and_the_Philosopher's_Stone_banner.jpg", TrailerUrl = "https://www.youtube.com/watch?v=VyHV0BRtdxo", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 2, Title = "Harry Potter and the Chamber of Secrets", Genre = "fantasy", ReleaseYear = 2002, Director = "Chris Columbus", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/c/c0/Harry_Potter_and_the_Chamber_of_Secrets_movie.jpg", TrailerUrl = "https://www.youtube.com/watch?v=s4Fh2WQ2Xbk", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 3, Title = "Harry Potter and the Prisoner of Azkaban", Genre = "fantasy", ReleaseYear = 2002, Director = "Chris Columbus", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/b/bc/Prisoner_of_azkaban_UK_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=1ZdlAg3j8nI", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 4, Title = "Fantastic Beasts and Where To Find Them", Genre = "fantasy", ReleaseYear = 2016, Director = "David Yates", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/5/5e/Fantastic_Beasts_and_Where_to_Find_Them_poster.png", TrailerUrl = "https://www.youtube.com/watch?v=ViuDsy7yb8M", FranchiseId = 1 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 5, Title = "Batman", Genre = "action,fantasy", ReleaseYear = 1989, Director = "Tim Burton", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/5/5a/Batman_%281989%29_theatrical_poster.jpg", TrailerUrl = "https://www.youtube.com/watch?v=dgC9Q0uhX70", FranchiseId = 2 });
            modelBuilder.Entity<Movie>().HasData(new Movie() { Id = 6, Title = "The Dark Knight", Genre = "action,fantasy", ReleaseYear = 2008, Director = "Christopher Nolan", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/1/1c/The_Dark_Knight_%282008_film%29.jpg", TrailerUrl = "https://www.youtube.com/watch?v=EXeTwQWrcwY", FranchiseId = 2 });

            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, FullName = "Harry James Potter", Alias = "The Boy Who Lived", Gender = "male", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/d/d7/Harry_Potter_character_poster.jpg",  });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, FullName = "Albus Percival Wulfric Brian Dumbledore", Alias = null, Gender = "male", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/f/fe/Dumbledore_and_Elder_Wand.JPG" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, FullName = "Newt Scamander", Alias = null, Gender = "male", PictureUrl = "https://static.wikia.nocookie.net/harrypotter/images/3/36/Newton_Scamander_Profile_crop.png/revision/latest?cb=20190609204955" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, FullName = "Queenie Goldstein", Alias = null, Gender = "female", PictureUrl = "https://static.wikia.nocookie.net/warner-bros-entertainment/images/5/50/Queenie_Goldstein.jpg/revision/latest?cb=20190101201750" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, FullName = "Bruce Wayne", Alias = "Batman", Gender = "male", PictureUrl = "https://upload.wikimedia.org/wikipedia/en/c/c7/Batman_Infobox.jpg" });


            // Seed many-to-many relationship between Movies and Characters
            modelBuilder.Entity<Movie>()
                .HasMany(m => m.Characters)
                .WithMany(c => c.Movies)
                .UsingEntity<Dictionary<string, object>>(
                    "MovieCharacter",
                    r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    j =>
                    {
                        j.HasKey("MovieId", "CharacterId");
                        j.HasData(
                            new { MovieId = 1, CharacterId = 1 },
                            new { MovieId = 1, CharacterId = 2 },
                            new { MovieId = 2, CharacterId = 1 },
                            new { MovieId = 2, CharacterId = 2 },
                            new { MovieId = 3, CharacterId = 1 },
                            new { MovieId = 3, CharacterId = 2 },
                            new { MovieId = 4, CharacterId = 3 },
                            new { MovieId = 4, CharacterId = 4 },
                            new { MovieId = 5, CharacterId = 5 },
                            new { MovieId = 6, CharacterId = 5 }
                        );
                    });
        }
    }
}
