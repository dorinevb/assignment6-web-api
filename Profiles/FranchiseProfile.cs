﻿using AutoMapper;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTO;

namespace MovieCharacterAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>();
            CreateMap<FranchiseCreateDTO, Franchise>().ReverseMap();
            CreateMap<FranchiseEditDTO, Franchise>().ReverseMap();
        }
        
    }
}
