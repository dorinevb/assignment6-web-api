﻿using AutoMapper;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTO;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>();
            CreateMap<MovieCreateDTO, Movie>().ReverseMap();
            CreateMap<MovieEditDTO, Movie>().ReverseMap();
        }
    }
}
