﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTO;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/characters")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/characters
        /// <summary>
        /// Gets all the characters from the database.
        /// </summary>
        /// <returns>A list of Character Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }
            var characters = await _context.Characters.ToListAsync();
            var charactersReadDto = _mapper.Map<List<CharacterReadDTO>>(characters);
            return Ok(charactersReadDto);
        }

        // GET: api/characters/5
        /// <summary>
        /// Gets a character from the database by ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the character.</param>
        /// <returns>The Character Read DTO of the requested character.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            var characterReadDto = _mapper.Map<CharacterReadDTO>(character);
            return Ok(characterReadDto);
        }

        // PUT: api/characters/5
        /// <summary>
        /// Updates a character in the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the character.</param>
        /// <param name="characterEditDto">A Character Edit DTO.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO characterEditDto)
        {

            if (id != characterEditDto.Id)
            {
                return BadRequest();
            }
            Character characterDomain = _mapper.Map<Character>(characterEditDto);
            _context.Entry(characterDomain).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: api/characters
        /// <summary>
        /// Creates a new charater in the database.
        /// </summary>
        /// <param name="characterCreateDto">A Character Create DTO.</param>
        /// <returns>A Character Read DTO</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter(CharacterCreateDTO characterCreateDto)
        {
            var characterDomain = _mapper.Map<Character>(characterCreateDto);
            if (_context.Characters == null)
            {
                return Problem("Entity set 'MovieCharacterDbContext.Characters' is null.");
            }
            _context.Characters.Add(characterDomain);
            await _context.SaveChangesAsync();
            CharacterReadDTO newCharacterDto = _mapper.Map<CharacterReadDTO>(characterDomain);
            return CreatedAtAction("GetCharacter", new { id = newCharacterDto.Id }, newCharacterDto);
        }

        // DELETE: api/characters/5
        /// <summary>
        /// Removes a character from the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the character</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (_context.Characters == null)
            {
                return NotFound();
            }
            var character = await _context.Characters.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return (_context.Characters?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
