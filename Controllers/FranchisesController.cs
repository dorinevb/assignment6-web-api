﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTO;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/franchises")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(MovieCharacterDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        // GET: api/franchises
        /// <summary>
        /// Gets all the franchises from the database.
        /// </summary>
        /// <returns>A list of Franchise Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchises = await _context.Franchises.ToListAsync();
            var franchisesReadDto = _mapper.Map<List<FranchiseReadDTO>>(franchises);
            return Ok(franchisesReadDto);
        }

        // GET: api/franchises/5
        /// <summary>
        /// Gets a franchise from the database by ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A Franchise Read DTO.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            var franchiseReadDto = _mapper.Map<FranchiseReadDTO>(franchise);

            return Ok(franchiseReadDto);
        }

        // PUT: api/franchises/5
        /// <summary>
        /// Updates a franchise in the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <param name="franchiseEditDTO">A Franchise Edit DTO.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO franchiseEditDTO)
        {
            if (id != franchiseEditDTO.Id)
            {
                return BadRequest();
            }

            Franchise franchiseDomain = _mapper.Map<Franchise>(franchiseEditDTO);
            _context.Entry(franchiseDomain).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/franchises
        /// <summary>
        /// Creates a new franchise in the database.
        /// </summary>
        /// <param name="franchiseCreateDto">A Franchise Create DTO.</param>
        /// <returns>A Franchise Read DTO.</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise(FranchiseCreateDTO franchiseCreateDto)
        {
            var franchiseDomain = _mapper.Map<Franchise>(franchiseCreateDto);
            if (_context.Franchises == null)
            {
                return Problem("Entity set 'MovieCharacterDbContext.Franchises' is null.");
            }
            _context.Franchises.Add(franchiseDomain);
            await _context.SaveChangesAsync();

            FranchiseReadDTO newFranchiseDto = _mapper.Map<FranchiseReadDTO>(franchiseDomain);

            return CreatedAtAction("GetFranchise", new { id = newFranchiseDto.Id }, newFranchiseDto);
        }

        // DELETE: api/franchises/5
        /// <summary>
        /// Removes a franchise from the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (_context.Franchises == null)
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        // GET: api/franchises/5/movies
        // Get the movies of a given franchise.
        /// <summary>
        /// Gets all the movies of a franchise.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A list of Movie Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetFranchiseMovies(int id)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var moviesByFranchise = await _franchiseService.GetMoviesByFranchise(id);
            var moviesByFranchiseDto = _mapper.Map<List<MovieReadDTO>>(moviesByFranchise);
            return Ok(moviesByFranchiseDto);
        }

        // GET: api/franchises/5/characters
        // Get the characters of a given franchise.
        /// <summary>
        /// Gets all the characters of a franchise.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A list of Character Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<ICollection<CharacterReadDTO>>> GetFranchiseCharacters(int id)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }
            var charactersByFranchise = await _franchiseService.GetCharactersByFranchise(id);
            if (charactersByFranchise == null)
            {
                return NotFound();
            }
            // Map list of characters to list of character DTO's
            var charactersByFranchiseDto = _mapper.Map<List<CharacterReadDTO>>(charactersByFranchise);
            return Ok(charactersByFranchiseDto);
        }

        // PUT: api/franchises/1/movies
        // Update the movies of the given franchise.
        /// <summary>
        /// Updates the movies of a franchise.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <param name="movies">A list of integers representing the IDs of the movies.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!FranchiseExists(id))
            {
                return NotFound();
            }
            var franchiseForUpdate = await _franchiseService.GetFranchiseForUpdate(id);
            if (franchiseForUpdate == null)
            {
                return NotFound();
            }
            var moviesForUpdate = await _franchiseService.GetMoviesForUpdate(movies);
            if (moviesForUpdate == null)
            {
                return BadRequest("Movie does not exist.");
            }
            franchiseForUpdate.Movies = moviesForUpdate;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }

            return NoContent();
        }

        private bool FranchiseExists(int id)
        {
            return (_context.Franchises?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
