﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using MovieCharacterAPI.Models.DTO;
using MovieCharacterAPI.Services;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper, IMovieService movieService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
        }

        // GET: api/movies
        /// <summary>
        /// Gets all the movies from the database.
        /// </summary>
        /// <returns>A list of Movie Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movies = await _context.Movies.ToListAsync();
            var moviesReadDto = _mapper.Map<List<MovieReadDTO>>(movies);
            return Ok(moviesReadDto);
        }

        // GET: api/movies/5
        /// <summary>
        /// Gets a movie from the database by ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <returns>A Movie Read DTO.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            var movieReadDto = _mapper.Map<MovieReadDTO>(movie);
            return Ok(movieReadDto);
        }

        // PUT: api/movies/5
        /// <summary>
        /// Updates a movie in the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <param name="movieEditDto">A Movie Edit DTO.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieEditDto)
        {
            if (id != movieEditDto.Id)
            {
                return BadRequest();
            }
            Movie movieDomain = _mapper.Map<Movie>(movieEditDto);
            _context.Entry(movieDomain).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        // POST: api/movies
        /// <summary>
        /// Creates a new movie in the database.
        /// </summary>
        /// <param name="movieCreateDto">A Movie Create DTO.</param>
        /// <returns>A Movie Read DTO.</returns>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [HttpPost]
        public async Task<ActionResult<MovieReadDTO>> PostMovie(MovieCreateDTO movieCreateDto)
        {
            var movieDomain = _mapper.Map<Movie>(movieCreateDto);
            if (_context.Movies == null)
            {
                return Problem("Entity set 'MovieCharacterDbContext.Movies' is null.");
            }
            _context.Movies.Add(movieDomain);
            await _context.SaveChangesAsync();
            MovieReadDTO newMovieDto = _mapper.Map<MovieReadDTO>(movieDomain);
            return CreatedAtAction("GetMovie", new { id = newMovieDto.Id }, newMovieDto);
        }

        // DELETE: api/movies/5
        /// <summary>
        /// Removes a movie from the database.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (_context.Movies == null)
            {
                return NotFound();
            }
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // GET: api/movies/5/characters
        // Get the characters of a given movie.
        /// <summary>
        /// Gets all the charaters of a movie.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <returns>A list of Character Read DTO's.</returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<ICollection<CharacterReadDTO>>> GetMovieCharacters(int id)
        {
            if (!MovieExists(id))
            {
                return NotFound();
            }
            var movie = await _context.Movies.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }
            var charactersByMovie = await _movieService.GetCharactersByMovie(id);
            var charactersByMovieDto = _mapper.Map<List<CharacterReadDTO>>(charactersByMovie);
            return Ok(charactersByMovieDto);
        }

        // PUT: api/movies/5/characters
        // Update the characters of the given movie.
        /// <summary>
        /// Updates the characters of a movie.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <param name="characters">A list of integers representing the IDs of the characters.</param>
        /// <returns>An action result without content (status 204).</returns>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!MovieExists(id))
            {
                return NotFound();
            }
            var movieForUpdate = await _movieService.GetMovieForUpdate(id);
            var charactersForUpdate = await _movieService.GetCharactersForUpdate(characters);
            if (charactersForUpdate == null)
            {
                return BadRequest("Character does not exist.");
            }
            movieForUpdate.Characters = charactersForUpdate;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return NoContent();
        }

        private bool MovieExists(int id)
        {
            return (_context.Movies?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
