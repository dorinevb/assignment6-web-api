﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieCharacterDbContext _context;
        public FranchiseService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all the movies of a franchise.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A list of Movie objects.</returns>
        public async Task<ICollection<Movie>> GetMoviesByFranchise(int id)
        {
            var moviesByFranchise = await _context.Movies
                                        .Where(m => m.FranchiseId == id)
                                        .ToListAsync();
            // Prevent object cycle by setting movie.Franchise to empty object
            foreach (Movie mov in moviesByFranchise)
            {
                mov.Franchise = new Franchise();
            }
            return moviesByFranchise;
        }

        /// <summary>
        /// Gets all the characters of a franchise.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A list of Character objects.</returns>
        public async Task<ICollection<Character>?> GetCharactersByFranchise(int id)
        {
            // Get all characters from movies that belong to franchise
            // Nested list is stored in 'characters'
            var characters = await _context.Movies
                                    .Where(m => m.FranchiseId == id)
                                    .Select(m => m.Characters)
                                    .ToListAsync();
            // Use dictionary to avoid having multiple instances of the same character
            Dictionary<int, Character> characterDict = new Dictionary<int, Character>();
            // Loop through nested list and add characters to dictionary
            foreach (var sublist in characters)
            {
                foreach (var item in sublist)
                {
                    if (item != null)
                    {
                        characterDict[item.Id] = item;
                    }
                }
            }
            // Create list of characters
            List<Character> charactersByFranchise = new List<Character>();
            if (characterDict.Count < 1)
            {
                return null; // If dict was empty, return null
            }
            charactersByFranchise = characterDict.Values.ToList();
            return charactersByFranchise; // return list with character objects
        }

        /// <summary>
        /// Gets a franchise by ID, including the .Movies property.
        /// </summary>
        /// <param name="id">An integer representing the ID of the franchise.</param>
        /// <returns>A Franchise object.</returns>
        public async Task<Franchise?> GetFranchiseForUpdate(int id)
        {
            var franchiseForUpdate = await _context.Franchises
                                            .Include(f => f.Movies)
                                            .Where(f => f.Id == id)
                                            .FirstAsync();
            return franchiseForUpdate;
        }

        /// <summary>
        /// Creates a list of movies that can be added to a franchise.
        /// </summary>
        /// <param name="movies">A list of integers representing movie IDs.</param>
        /// <returns>A list of Movie objects.</returns>
        public async Task<ICollection<Movie>?> GetMoviesForUpdate(List<int> movies)
        {
            List<Movie> franchiseMovies = new();
            foreach (int movId in movies)
            {
                Movie mov = await _context.Movies.FindAsync(movId);
                if (mov == null)
                {
                    return null; // return null if one of the movies is null, so controller can return BadRequest.
                }
                franchiseMovies.Add(mov);
            }
            return franchiseMovies;
        }
    }
}
