﻿using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Services
{
    public interface IFranchiseService
    {
        public Task<ICollection<Movie>> GetMoviesByFranchise(int id);
        public Task<ICollection<Character>?> GetCharactersByFranchise(int id);
        public Task<Franchise?> GetFranchiseForUpdate(int id);
        public Task<ICollection<Movie>?> GetMoviesForUpdate(List<int> movies);
    }
}
