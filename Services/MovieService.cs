﻿using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.Models;
using MovieCharacterAPI.Models.Domain;
using System.Collections.ObjectModel;

namespace MovieCharacterAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieCharacterDbContext _context;
        public MovieService(MovieCharacterDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Gets all the characters of a movie.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <returns>A list of Character objects.</returns>
        public async Task<ICollection<Character>> GetCharactersByMovie(int id)
        {
            var charactersByMovie = await _context.Characters
                                        .Include(c => c.Movies)
                                        .Where(c => c.Movies
                                        .Any(m => m.Id == id))
                                        .ToListAsync();
            // Prevent object cycle by setting character.Movies to empty collection
            foreach (Character ch in charactersByMovie)
            {
                ch.Movies = new Collection<Movie>();
            }
            return charactersByMovie;
        }

        /// <summary>
        /// Gets a movie by ID.
        /// </summary>
        /// <param name="id">An integer representing the ID of the movie.</param>
        /// <returns>A Movie object</returns>
        public async Task<Movie?> GetMovieForUpdate(int id)
        {
            Movie movieForUpdate = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.Id == id)
                .FirstAsync();
            return movieForUpdate;
        }

        /// <summary>
        /// Creates a list of characters that can be added to a movie.
        /// </summary>
        /// <param name="characters">A list of integers representing the IDs of characters.</param>
        /// <returns>A list of Character objects.</returns>
        public async Task<ICollection<Character>?> GetCharactersForUpdate(List<int> characters)
        {
            List<Character> movieCharacters = new();
            foreach (int chId in characters)
            {
                Character ch = await _context.Characters.FindAsync(chId);
                if (ch == null)
                    return null;
                movieCharacters.Add(ch);
            }
            return movieCharacters;
        }
    }
}
