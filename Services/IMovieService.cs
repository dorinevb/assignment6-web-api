﻿using MovieCharacterAPI.Models.Domain;

namespace MovieCharacterAPI.Services
{
    public interface IMovieService
    {
        public Task<ICollection<Character>> GetCharactersByMovie(int id);
        public Task<Movie?> GetMovieForUpdate(int id);
        public Task<ICollection<Character>?> GetCharactersForUpdate(List<int> characters);
    }
}
