# Web API Assignment

This ASP.NET Web API was written in C# and uses .NET 6.0 and Microsoft SQL Server. The API allows users to perform CRUD operations on a database with movie characters.

## Background

I created this Web API as an assignment for the backend course of Noroff School of Technology and Digital Media. The course is part of the software development traineeship at Experis (Manpower Group). The goal of this assignment is to learn to create an Entity Framework code first workflow and to build an ASP.NET Web API, as well as using AutoMapper to map domain models to DTOs (Data Transfer Objects) and using Swagger for documentation.

## Install

This application can be run in Visual Studio 2022. The app uses Microsoft SQL Server to manage the database. Make sure to have Microsoft SQL Server Management Studio (SSMS) installed on your computer. When you start SMSS, copy the servername and paste it in the 'DefaultConnection' string in the appsettings.json file of this application:    
`"DefaultConnection": "Data Source={your-servername}; Initial Catalog=MovieCharacterDb; Integrated Security=True;"`

## Usage

The database has 4 tables:
1. Characters: this table contains the movie characters. A character can be in more than one movie.
2. Movies: this table contains the movies in which the characters occur. A movie can have more than one character. A movie can only be a part of one franchise.
3. Franchises: this table contains the franchises that the movies belong to.
4. MovieCharacter: this joining table contains the many-to-many relationship between movies and characters.

Upon running the app, the API will open in the Swagger platform. Here you can clearly see all the CRUD operations you can perform on the Characters, Movies and Franchises. The documentation explains what the operations do and which types of responses you can expect. By clicking on 'Try it out' you can perform the operations. Please note that by deleting an item, it is removed permanently from the database.

## License

MIT

Copyright (c) 2022 Dorine van Belzen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
